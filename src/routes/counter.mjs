import { Router } from 'express';
import Counter from "../controllers/counter.mjs"

const app = Router();

const counter = new Counter();

if (counter instanceof Counter) {
    app.get('/read', counter.read);
    app.get('/increase', counter.increase);
    app.get('/zero', counter.zero);
}

export default app;
