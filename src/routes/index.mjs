import { Router } from 'express';
import counter from './counter.mjs';

const app = Router();

app.get('', (req, res) => {
    res.send('welcome!');
});

app.get('hello', () => {
    res.send('Hello!');
})

app.get('/bye', () => {
    res.send('Bye!');
});

app.all('*', (req, res) => {
    res.redirect('/');
});
  
app.get('/', (req, res) => {
    res.send('welcome!');
});

app.use('/counter', counter);

export default app;