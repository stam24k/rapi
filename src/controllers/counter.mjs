export default class CounterController {
    /** @type {number} */
    count;
    constructor() {
        CounterController.count = 0;
    }
    async increase(req, res, next) {
        CounterController.count++;
        res.send(`counter: ${CounterController.count}`);
    }
    async read(req, res, next) {
        res.send(`counter: ${CounterController.count}`);
    }
    async zero(req, res, next) {
        CounterController.count = 0;
        res.send(`counter: ${CounterController.count}`);
    }
}
