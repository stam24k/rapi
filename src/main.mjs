import express from 'express';
import router from './router.mjs';

const PORT = 3000;

const app = express();

app.use('', router);

app.listen(PORT, () => {
    console.log(`Server listening at http://127.0.0.1:${PORT}`);
});
