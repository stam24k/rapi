import { Router } from 'express';
import routes from './routes/index.mjs';

const app = Router()

app.use('/api/v1', routes);

export default app;