#!/bin/bash

npm init -y
git init

npm i --save express
npm i --save-dev chai mocha

mkdir src
touch src/main.mjs
touch src/router.mjs

mkdir src/routes
touch src/routes/counter.mjs

# NPM project details
npm pkg set description="Random REST API"
npm pkg set author="team_x"
npm pkg set license="MIT"

# ECMAScript approach
npm pkg set type="module"

# NPM scripts
npm pkg set scripts.start="node src/main.mjs"
npm pkg set scripts.test="mocha"
npm pkg set scripts.dev="node --watch-path=src --watch src/main.mjs"

# Gitignore
echo ".DS_Store" >> .gitignore
echo "node_modules" >> .gitignore
